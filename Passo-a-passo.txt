# DIA 3 # Jenkins

- Criar um job de exemplo no Jenkins, ensinando sobre parametros, e dando um echo na tela. (~10m)
- Agora vamos automatizar todo aquele processo de build que fizemos na mão ontem, criar o job com o pipeline groovy. (~)
- Criado o job, antes de executa-lo vamos criar a credential para acesso ao Docker Hub (~3m)
- Ainda antes de executar o job, instalar o plugin 'Docker Pipeline'
- Rodar os comandos dentro da instância do Jenkins
- Rodar o pipeline no Jenkins e mostrar que a versão nova subiu no docker hub
- Alterar algo no gitlab, rodar o pipeline novamente para gerar a nova imagem, e rodar manualmente na instancia a nova versão. (src/components/cards.jsx)

# DIA 3 # CodeDeploy

- Acessar o IAM e criar uma role para o CodeDeploy com a policy default para o CodeDeploy
- Acessar o IAM e criar uma role para o EC2 (S3ReadOnly) com a policy S3ReadOnlyAccess
- No EC2, vincular a função S3ReadOnly a instancia app-server
- Acessar a instancia app-server e rodar os comandos para instalar o agente do CodeDeploy
- Criar o bucket no S3 na mesma região das instâncias.
- Agora no CodeDeploy, crie o aplicativo e então o grupo de implantação.
- Modificar o appspec e os scripts de stop e start.
- Modificar o pipeline  no Jenkinis aonde tiver BUILD_NUMBER para develop e rodar o pipeline.
- Pegar o appspec e os scripts localmente e compactar em zip e mandar para o bucket s3, copie o URI S3.
- Agora no CodeDeploy crie uma implantação apontando para este zip e veja a magica acontecer.
- Agora modifique alguma coisa no GIT, execute o Jenkins e após isso execute uma nova implantação no CodeDeploy.

